Name:           usbview
Version:        1.1
Release:        3%{?dist}
Summary:        USB topology and device viewer

Group:          Applications/System
License:        GPLv2
URL:            http://www.kroah.com/linux-usb/
Source0:        http://www.kroah.com/linux-usb/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel

Source1:        %{name}_icon.png
Source2:        %{name}.desktop


BuildRequires:  desktop-file-utils


%description
Display information about the topology of the devices connected to the USB bus
on a Linux machine. It also displays detailed information on the individual
devices.

%prep
%setup -q
# Convert to utf-8
for file in ChangeLog; do
    iconv -f ISO-8859-1 -t UTF-8 -o $file.new $file && \
    touch -r $file $file.new && \
    mv $file.new $file
done

%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

#install desktop icon
install -Dp -m 0644 %{SOURCE1} $RPM_BUILD_ROOT%{_datadir}/pixmaps/%{name}_icon.png

#install desktop file
desktop-file-install                                    \
--add-category="Development"                            \
--dir=$RPM_BUILD_ROOT%{_datadir}/applications           \
%{SOURCE2}

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc ChangeLog COPYING* README TODO
%{_bindir}/usbview
%{_mandir}/man8/usbview*
%{_datadir}/pixmaps/%{name}_icon.png
%{_datadir}/applications/%{name}.desktop

%changelog
* Fri Dec 11 2009 Itamar Reis Peixoto <itamar@ispbrasil.com.br> - 1.1-3
- increasy usage of macros, fix icon permission

* Fri Dec 11 2009 Itamar Reis Peixoto <itamar@ispbrasil.com.br> - 1.1-2
- install desktop icon

* Sun Nov 29 2009 Itamar Reis Peixoto <itamar@ispbrasil.com.br> - 1.1-1
- Initial RPM for fedora

